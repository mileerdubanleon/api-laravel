<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    public function clients(){
        return $this->belongToManyRole(Client::class, 'clients_services');
    }
}
